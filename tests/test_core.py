import time
from typing import List
import bridge
from cuba import get_client

from bridge import Line, Order


cli = get_client()

# c = bter.Contract(symbol="000001.SH")
# l = bter.Line("000001.SH", 1, 1, 1, 1, 1, 1, datetime.datetime.now())
# print(l)
# print(bter.Direction.LONG.against())
order = bridge.Order("000001.SZ", 0, 0, bridge.Offset.OPEN, bridge.Direction.LONG, bridge.OrderType.MARKET)
print(order)
# print(bter.Offset.OPEN)
broker = bridge.Broker()
# print(broker.orders)


class Strategy(bridge.BaseStrategy):
    def next(self, line: Line, is_last: bool) -> List[Order]:
        return super().next(line, is_last)


broker = bridge.Broker()
broker.set_price_mode(bridge.beans.CLOSE)
broker.set_cash(100000000)
print(broker.account)
data = cli.read("stock::pv", start="2023-06-01").reset_index()
data = [bridge.Line(x.symbol, x.date, x.open, x.high, x.low, 0.0, x.vol) for x in data.itertuples()]
print(data[0])
print(data[0].symbol)
broker.set_data(data)
strategy = Strategy()
broker.set_strategy(strategy)
t2 = time.time()
broker.run()
t3 = time.time()
print(f"backtest costs: {round(t3-t2, 2)} seconds")
print(broker.get_nav())
