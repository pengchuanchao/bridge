import datetime
import dataclasses
from enum import Enum
from typing import List, Dict
from .performance import Performance


class PriceMode(Enum):
    OPEN = 0
    HIGH = 1
    LOW = 2
    CLOSE = 3


class Direction(Enum):
    LONG = "LONG"
    SHORT = "SHORT"

    def against(self) -> Direction:
        ...
    
    def __repr__(self) -> str:
        ...
    
    def __str__(self) -> str:
        ...
    

class Offset(Enum):
    OPEN = "open"
    CLOSE = "close"


class OrderType:
    LIMIT = "limit"
    MARKET = "market" 
    FAK = "fak" 
    FOK = "fok"


class Status(Enum):
    TRADED = "traded" 
    REJECT = "reject"


@dataclasses.dataclass
class Contract:
    symbol: str
    commission_buy: float = 0.001
    commission_sell: float = 0.001
    limit: float = 0.01


@dataclasses.dataclass
class Line:
    symbol: str
    open: float
    high: float
    low: float
    close: float
    oi: float
    volume: float
    datetime: datetime.datetime


@dataclasses.dataclass
class Order:
    symbol: str
    price: float
    volume: int
    offset: Offset
    direction: Direction
    order_type: OrderType

    def vt_symbol(self) -> str:
        ...


@dataclasses.dataclass
class Trade:
    symbol: str
    price: float
    volume: int
    offset: Offset
    direction: Direction
    status: Status

    @staticmethod
    def from_order(order: Order) -> Trade:
        ...


@dataclasses.dataclass
class Position:
    symbol: str
    amount: float
    direction: Direction
    volume: int
    cost: float
    pnl: float

    @staticmethod
    def from_trade(trade: Trade) -> Position:
        ...

    def on_trade(self, trade: Trade):
        ...

    def on_market(self, line: Line):
        ...


@dataclasses.dataclass
class Account:
    balance: float
    frozen: float
    available: float
    fee: float


@dataclasses.dataclass
class Nav:
    dt: datetime.datetime
    nav: float


class Engine:
    def get_account(self) -> Account:
        ...

    def get_trades(self) -> List[Trade]:
        ...

    def get_positions(self) -> Dict[str, Position]:
        ...

    def get_orders(self) -> List[Order]:
        ...

    def get_nav(self) -> List[Nav]:
        ...

    def accept_orders(self, orders: List[Order]) -> None:
        ...


class BaseStrategy:

    def set_engine(self, engine: Engine) -> None:
       """set backtest broker engine"""
       ...

    def get_account(self) -> Account:
        """get current account info"""
        ...

    def get_trades(self) -> List[Trade]:
        """get traded order info"""
        ...

    def get_positions(self) -> Dict[str, Position]:
        """get current position info"""
        ...

    def get_orders(self) -> List[Order]:
        """get untraded orders"""
        ...

    def get_nav(self) -> List[Nav]:
        """get strategy net values"""
        ...

    def next(self, line: Line, is_last: bool) -> List[Order]:
        """call by broker engine, implement your strategy here"""
        ...

    @property
    def account(self) -> Account:
        ...
    
    @property
    def positions(self) ->Dict[str, Position]:
        ...
    
    @property
    def trades(self) -> List[Trade]:
        ...
    
    @property
    def orders(self) -> List[Order]:
        ...
    
    @property
    def nav(self) -> List[Nav]:
        ...


class Broker(Engine):

    def __cinit__(self):
        ...

    def set_price_mode(self, mode: PriceMode) -> None:
        ...

    def set_cash(self, cash: float) -> None:
        ...

    def set_contract(self, contract: Contract) -> None:
        ...

    def set_data(self, data: List[Line]) -> None:
        ...

    def set_strategy(self, strategy: BaseStrategy) -> None:
        ...

    def broadcast(self) -> None:
        ...

    def run(self) -> None:
        self.broadcast()

    def match(self, line: Line) -> List[Trade]:
        ...

    def trade(self, line: Line, order: Order) -> Trade:
        ...

    def buy_open(self, line: Line, order: Order) -> Trade:
        ...

    def sell_open(self, line: Line, order: Order) -> Trade:
        ...
    
    def buy_close(self, line: Line, order: Order) -> Trade:
        ...
    
    def sell_close(self, line: Line, order: Order) -> Trade:
        ...
    
    def last_price(self, line: Line):
        ...
        
    def closable(self, order: Order) -> bool:
        ...

    def set_last_price(self, line: Line) -> None:
        ...

    def get_last_price(self, symbol: str) -> Line:
        ...
    
    def get_contract(self, symbol: str) -> Contract:
        ...

    def update_position_by_trade(self, trade: Trade) -> None:
        ...

    def update_position_by_market(self, line: Line) -> bool:
        ...
    
    def update_account_by_trade(self, trade) -> None:
        ...

    def update_account(self) -> None:
        ...

    def update_nav(self, date: datetime.datetime) -> None:
        ...

    def get_account(self) -> Account:
        ...

    def get_trades(self) -> List[Trade]:
        ...
    
    def get_orders(self) -> List[Order]:
        ...

    def get_positions(self) -> Dict[str, Position]:
        ...

    def get_nav(self) -> List[Nav]:
        ...

    def accept_orders(self, orders: List[Order]) -> None:
        ...