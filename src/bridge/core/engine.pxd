from .beans cimport Account


cdef class Engine:
    cdef Account get_account(self) except *

    cdef list get_trades(self) except *

    cdef dict get_positions(self) except *

    cdef dict get_orders(self) except *

    cdef list get_nav(self) except *

    cdef void accept_orders(self, list orders) except *