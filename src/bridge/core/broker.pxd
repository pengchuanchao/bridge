cimport cython
from cpython cimport datetime

from .beans cimport (
    against, PriceMode, Direction, Offset, Status, OrderType, 
    Contract, Line, Order, Trade, Account, Position, Nav
)
from .strategy cimport BaseStrategy
from .engine cimport Engine


cdef class Broker(Engine):
    cdef dict __dict__
    cdef PriceMode mode
    cdef list lines
    cdef BaseStrategy strategy
    cdef public Account account
    cdef public dict contracts
    cdef public dict positions
    cdef public dict orders
    cdef public list trades
    cdef public list nav
    cdef public dict last_prices

    cpdef void set_price_mode(self, PriceMode mode) except *

    cpdef void set_cash(self, double cash) except *

    cpdef void set_contract(self, Contract contract) except *

    cpdef void set_data(self, list data) except *

    cpdef void set_strategy(self, BaseStrategy strategy) except *

    cdef void broadcast(self) except *

    cpdef void run(self) except *

    cdef list sort_orders(self) except *

    cdef list match(self, Line line) except *

    cdef Trade trade(self, Line line, Order order) except *

    cdef Trade buy_open(self, Line line, Order order) except *

    cdef Trade sell_open(self, Line line, Order order) except *
    
    cdef Trade buy_close(self, Line line, Order order) except *
    
    cdef Trade sell_close(self, Line line, Order order) except *
    
    cdef double last_price(self, Line line) except *

    cdef bint closable(self, Order order) except *

    cdef void set_last_price(self, Line line) except *

    cdef Line get_last_price(self, str symbol) except *
    
    cdef Contract get_contract(self, str symbol) except *

    cdef void update_position_by_trade(self, Trade trade) except *

    cdef bint update_position_by_market(self, Line line) except *
    
    cdef void update_account_by_trade(self, Trade trade) except *

    cdef void update_account(self) except *

    cdef void update_nav(self, datetime.datetime date) except *

    cpdef Account get_account(self) except *

    cpdef list get_trades(self) except *
    
    cpdef dict get_orders(self) except *

    cpdef dict get_positions(self) except *

    cpdef list get_nav(self) except *

    cpdef void accept_orders(self, list orders) except *



cdef inline str sort_by_symbol(Line line) except *:
    return line.symbol


cpdef inline datetime.datetime sort_by_date(Line line) except *:
    return line.date


cdef inline tuple sort_by_date_symbol(Line line) except *:
        return (line.date, line.symbol)


cdef inline str sort_order_by_symbol(Order order) except *:
    return order.symbol
