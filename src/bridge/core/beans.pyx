cimport cython
import dataclasses


@dataclasses.dataclass
cdef class Contract:
    symbol: str
    commission_buy: cython.double = 0.001
    commission_sell: cython.double = 0.001
    limit: cython.double = 0.01


@dataclasses.dataclass
cdef class Line:
    symbol: str
    date: datetime.datetime
    open: cython.double
    high: cython.double
    low: cython.double
    close: cython.double
    oi: cython.double = 0
    volume: cython.double = 0


@dataclasses.dataclass
cdef class Order:
    symbol: str
    price: cython.double
    volume: cython.ulong
    offset: Offset
    direction: Direction
    order_type: OrderType

    cdef str vt_symbol(self) except *:
        return "{}.{}".format(self.symbol, self.direction)


@dataclasses.dataclass
cdef class Trade:
    symbol: str
    price: cython.double
    volume: cython.ulong
    offset: Offset
    direction: Direction
    status: Status

    cdef str vt_symbol(self) except *:
        return "{}.{}".format(self.symbol, self.direction)

    @staticmethod
    cdef Trade from_order(Order order) except *:
        return Trade(order.symbol, order.price, order.volume, order.offset, order.direction, Status.REJECT)


@dataclasses.dataclass
cdef class Position:
    symbol: str
    amount: cython.double
    direction: Direction
    volume: cython.ulong
    cost: cython.double
    pnl: cython.double

    @staticmethod
    cdef Position from_trade(Trade trade) except *:
        amount = cost = trade.price * trade.volume
        return Position(trade.symbol, amount, trade.direction, trade.volume, cost, 0.0)

    cpdef void on_trade(self, Trade trade) except *:
        if trade.offset == Offset.OPEN:
            self.volume += trade.volume
            self.cost += trade.price * trade.volume
        else:
            self.volume -= trade.volume
            self.cost -= trade.price * trade.volume
        self.amount = self.volume * trade.price
        self.pnl = self.amount - self.cost

    cpdef void on_market(self, Line line) except *:
        self.amount = self.volume * line.close
        self.pnl = self.amount - self.cost


@dataclasses.dataclass
cdef class Account:
    balance: cython.double
    frozen: cython.double
    available: cython.double
    fee: cython.double = 0.0

    @staticmethod
    cdef Account new(double cash) except *:
        return Account(cash, 0, cash, 0)


@dataclasses.dataclass
cdef class Nav:
    date: datetime.datetime
    nav: cython.double