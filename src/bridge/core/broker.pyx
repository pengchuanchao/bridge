import cython

from typing import List, Dict
from itertools import groupby


cdef class Broker(Engine):
    __dict__: dict
    mode: PriceMode
    strategy: BaseStrategy
    lines: List[Line]
    account: Account
    contracts: Dict[str, Contract]
    positions: Dict[str, Position]
    orders: Dict[str, Order]
    trades: List[Trade]
    nav: List[Nav]
    last_prices: Dict[str, Line]
    
    def __cinit__(self):
        self.mode = PriceMode.Open
        self.contracts = {}
        self.positions = {}
        self.trades = []
        self.orders = {}
        self.nav = []
        self.last_prices = {}

    cpdef void set_price_mode(self, PriceMode mode) except *:
        self.mode = mode

    cpdef void set_cash(self, double cash) except *:
        self.account = Account.new(cash)

    cpdef void set_contract(self, Contract contract) except *:
        self.contracts[contract.symbol] = contract

    cpdef void set_data(self, List[Line] data) except *:
        data = sorted(data, key=sort_by_date_symbol)
        grouped = groupby(data, key=sort_by_date)
        self.lines = [(x[0], list(x[1])) for x in grouped]

    cpdef void set_strategy(self, BaseStrategy strategy) except *:
        strategy.set_engine(self)
        self.strategy = strategy

    cdef void broadcast(self) except *:
        date: datetime.datetime
        line: Line
        lines: List[Line]
        trades: List[Trade]
        orders: List[Order]
        for (date, lines) in self.lines:
            # line resort
            lines_m: dict = {x.symbol: x for x in lines}
            ordered = self.sort_orders()
            lines = [lines_m.pop(x) for x in ordered if x in lines_m]
            unorderd = sorted(lines_m.values(), key=sort_by_symbol)
            lines.extend(unorderd)
            length: cython.uint = len(lines)
            for idx, line in enumerate(lines):
                self.set_last_price(line)
                # trade
                trades = self.match(line)
                if trades:
                    for trade in trades:
                        self.update_position_by_trade(trade)
                        self.update_account_by_trade(trade)

                # 为什么还要更新position？ trade价格不一定是收盘价，但是日结算必须是收盘价
                updated: bool = self.update_position_by_market(line)
                if updated:
                    self.update_account()
                orders = self.strategy.next(line, idx == length-1)
                self.accept_orders(orders)
            self.update_nav(date)

    cpdef void run(self) except *:
        self.broadcast()

    cdef List[str] sort_orders(self) except *:
        orders: List[Order] = list(self.orders.values())
        orders = sorted(orders, key=sort_order_by_symbol)
        orders = sorted(orders, key=lambda x: x.offset.value, reverse=True)
        return [x.symbol for x in orders]

    cdef List[Trade] match(self, Line line) except *:
        keys = ["{}.{}".format(line.symbol, Offset.OPEN), "{}.{}".format(line.symbol, Offset.CLOSE)]
        trades: List[Trade] = []
        for key in keys:
            order = self.orders.pop(key, None)
            if not order:
                continue
            trade = self.trade(line, order)
            trades.append(trade)
        return trades

    cdef Trade trade(self, Line line, Order order) except *:
        if order.offset == Offset.OPEN:
            if order.direction == Direction.LONG:
                trade = self.buy_open(line, order)
            else:
                trade = self.sell_open(line, order)
        else:
            if order.direction == Direction.LONG:
                trade = self.buy_close(line, order)
            else:
                trade = self.sell_close(line, order)
        self.trades.append(trade)
        return trade

    cdef Trade buy_open(self, Line line, Order order) except *:
        lp = self.last_price(line)
        trade = Trade.from_order(order)
        if order.order_type == OrderType.LIMIT:
            # 买入价低于卖出价
            if order.price < lp:
                return trade
    
        trade.price = lp
        amount = lp * order.volume
        if self.account.available < amount:
            return trade
        trade.status = Status.TRADED
        return trade

    cdef Trade sell_open(self, Line line, Order order) except *:
        lp = self.last_price(line)
        trade = Trade.from_order(order)
        if order.order_type == OrderType.LIMIT:
            # 卖出价高于买入价
            if order.price > lp:
                return trade
    
        trade.price = lp
        amount = lp * order.volume
        if self.account.available < amount:
            return trade
        trade.status = Status.TRADED
        return trade
    
    cdef Trade buy_close(self, Line line, Order order) except *:
        lp = self.last_price(line)
        trade = Trade.from_order(order)
        if order.order_type == OrderType.LIMIT:
            # 买入价低于卖出价
            if order.price < lp:
                return trade
        if not self.closable(order):
            return trade
        trade.price = lp
        trade.status = Status.TRADED
        return trade
    
    cdef Trade sell_close(self, Line line, Order order) except *:
        lp = self.last_price(line)
        trade = Trade.from_order(order)
        if order.order_type == OrderType.LIMIT:
            # 卖出价高于买入价
            if order.price > lp:
                return trade
        if not self.closable(order):
            return trade
        trade.price = lp
        trade.status = Status.TRADED
        return trade
    
    cdef double last_price(self, Line line) except *:
        if self.mode == PriceMode.Open:
            return line.open
        elif self.mode == PriceMode.High:
            return line.high
        elif self.mode == PriceMode.Low:
            return line.low
        else:
            return line.close
        
    cdef bint closable(self, Order order) except *:
        against_ = "{}.{}".format(order.symbol, against(order.direction))
        if against_ not in self.positions:
            return False
        p = self.positions.get(against_)
        return order.volume <= p.volume

    cdef void set_last_price(self, Line line) except *:
        self.last_prices[line.symbol] = line

    cdef Line get_last_price(self, str symbol) except *:
        return self.last_prices[symbol]
    
    cdef Contract get_contract(self, str symbol) except *:
        if symbol in self.contracts:
            return self.contracts[symbol]
        contract = Contract(symbol)
        self.set_contract(contract)
        return contract

    cdef void update_position_by_trade(self, Trade trade) except *:
        if trade.status == Status.REJECT:
            return
        key = trade.vt_symbol()
        if trade.offset == Offset.OPEN:
            # 开仓
            p = self.positions.get(key)
            if p:
                p.on_trade(trade)
            else:
                self.positions[key] = Position.from_trade(trade)
        else:
            # 平仓
            against_ = "{}.{}".format(trade.symbol, against(trade.direction))
            p = self.positions.get(against_)
            p.on_trade(trade)

    cdef bint update_position_by_market(self, Line line) except *:
        updated = False
        keys = ["{}.{}".format(line.symbol, Direction.LONG), "{}.{}".format(line.symbol, Direction.SHORT)]
        for key in keys:
            position = self.positions.get(key, None)
            if position:
                position.on_market(line)
                updated = True
        return updated
    
    cdef void update_account_by_trade(self, Trade trade) except *:
        if trade.status == Status.REJECT:
            return
        c = self.get_contract(trade.symbol)
        if trade.offset == Offset.OPEN:
            ratio = c.commission_buy
        else:
            ratio = c.commission_sell
        amount = trade.price * trade.volume
        fee = amount * ratio
        acc = self.account
        acc.balance -= fee
        acc.fee += fee
        if trade.offset == Offset.OPEN:
            acc.available = acc.available - amount - fee
            acc.frozen += amount
        else:
            acc.available = acc.available + amount - fee
            acc.frozen -= amount
            if acc.frozen < 0.0:
                acc.frozen = 0.0
        self.account = acc

    cdef void update_account(self) except *:
        acc = self.account
        hold = sum([x.amount for x in self.positions.values()])
        acc.balance = acc.available + hold
        acc.frozen = hold
        self.account = acc

    cdef void update_nav(self, datetime.datetime date) except *:
        hold: cython.double = 0.0;
        for position in self.positions.values():
            symbol: str = position.symbol
            line = self.get_last_price(symbol)
            hold += line.close * position.volume
        self.nav.append(Nav(date, nav=self.account.available + hold))

    cpdef Account get_account(self) except *:
        return self.account

    cpdef List[Trade] get_trades(self) except *:
        return self.trades
    
    cpdef Dict[str, Order] get_orders(self) except *:
        return self.orders

    cpdef Dict[str, Position] get_positions(self) except *:
        return self.positions

    cpdef List[Nav] get_nav(self) except *:
        return self.nav

    cpdef void accept_orders(self, List[Order] orders) except *:
        for order in orders:
            key = "{}.{}".format(order.symbol, order.offset.value)
            self.orders[key] = order
