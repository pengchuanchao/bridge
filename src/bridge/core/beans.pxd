
from cpython cimport datetime


cpdef enum PriceMode:
    Open,
    High,
    Low,
    Close


cpdef enum Direction:
    LONG,
    SHORT


cdef inline Direction against(Direction direction) except *:
    if direction == Direction.LONG:
        return Direction.SHORT
    return Direction.LONG


cpdef enum OrderType:
    LIMIT,
    MARKET,
    FAK,
    FOK


cpdef enum Offset:
    OPEN,
    CLOSE


cpdef enum Status:
    TRADED,
    REJECT


cdef class Contract:
    cdef public str symbol
    cdef public double commission_buy
    cdef public double commission_sell
    cdef public double limit


cdef class Line:
    cdef public str symbol
    cdef public datetime.datetime date
    cdef public double open
    cdef public double high
    cdef public double low
    cdef public double close
    cdef public unsigned long oi
    cdef public unsigned long volume


cdef class Order:
    cdef public str symbol
    cdef public double price
    cdef public unsigned long volume
    cdef public Offset offset
    cdef public Direction direction
    cdef public OrderType order_type

    cdef str vt_symbol(self) except *


cdef class Trade:
    cdef public str symbol
    cdef public double price
    cdef public unsigned long volume
    cdef public Offset offset
    cdef public Direction direction
    cdef public Status status

    cdef str vt_symbol(self) except *

    @staticmethod
    cdef Trade from_order(Order order) except *


cdef class Position:
    cdef public str symbol
    cdef public double amount
    cdef public Direction direction
    cdef public unsigned long volume
    cdef public double cost
    cdef public double pnl

    @staticmethod
    cdef Position from_trade(Trade trade) except *

    cpdef void on_trade(self, Trade trade) except *

    cpdef void on_market(self, Line line) except *


cdef class Account:
    cdef public double balance
    cdef public double frozen
    cdef public double available
    cdef public double fee

    @staticmethod
    cdef Account new(double cash) except *


cdef class Nav:
    cdef public datetime.datetime date
    cdef public double nav
