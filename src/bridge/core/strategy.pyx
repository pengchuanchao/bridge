import cython
from typing import Dict, List

from .beans import Position, Trade, Order, Nav
from cython.cimports.bridge.core.engine import Engine  #type: ignore
from cython.cimports.bridge.core.beans import Account, Line  #type: ignore


cdef class BaseStrategy:

    engine: Engine

    cpdef void set_engine(self, engine: Engine) except * :
        self.engine = engine

    cpdef Account get_account(self) except * :
        return self.engine.get_account()
    
    cpdef Dict[str, Position] get_positions(self) except * :
        return self.engine.get_positions()
    
    cpdef List[Trade] get_trades(self) except * :
        return self.engine.get_trades()
    
    cpdef Dict[str, Order] get_orders(self) except * :
        return self.engine.get_orders()
    
    cpdef List[Nav] get_nav(self) except * :
        return self.engine.get_nav()
    
    cpdef List[Order] next(self, line: Line, is_last: cython.bint) except * :
        return []
    
    @property
    def account(self) -> Account:
        return self.get_account()
    
    @property
    def positions(self) ->Dict[str, Position]:
        return self.get_positions()
    
    @property
    def trades(self) -> List[Trade]:
        return self.get_trades()
    
    @property
    def orders(self) -> Dict[str, Order]:
        return self.get_orders()
    
    @property
    def nav(self) -> List[Nav]:
        return self.get_nav()