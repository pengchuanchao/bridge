import cython
import datetime
from typing import List, Dict

from .beans import Position, Trade, Nav, Order, Account


cdef class Engine:

    cdef Account get_account(self) except *:
        pass

    cdef List[Trade] get_trades(self) except *:
        pass

    cdef Dict[str, Position] get_positions(self) except *:
        pass

    cdef Dict[str, Order] get_orders(self) except *:
        pass

    cdef List[Nav] get_nav(self) except *:
        pass

    cdef void accept_orders(self, List[Order] orders) except *:
        pass
