cimport cython
from .beans cimport Account, Line
from .engine cimport Engine


cdef class BaseStrategy:
    cdef Engine engine

    cpdef void set_engine(self, Engine engine) except *

    cpdef Account get_account(self) except *

    cpdef list get_trades(self) except *

    cpdef dict get_positions(self) except *

    cpdef dict get_orders(self) except *

    cpdef list get_nav(self) except *

    cpdef list next(self, Line line, cython.bint is_last) except *
