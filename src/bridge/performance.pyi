from typing import List

import pandas as pd
import numpy as np

from bridge.core.beans import Nav


NO_RISK_RATIO: float


class Performance:
    """
    计算组合的评价指标
    Example:
    >>> perf = Performance(df)
    >>> perf.perf()
    """

    def __init__(self, data: pd.DataFrame):
        ...

    @staticmethod
    def from_navs(navs: List[Nav]) -> "Performance":
        """从回测净值导入"""
        ...

    def pct_change(self) -> np.ndarray:
        ...
    
    def growth(self) -> np.ndarray:
        """累计增长率"""
        ...
    
    def cagr(self) -> np.ndarray:
        """复合增长率"""
        ...

    def vol(self) -> np.ndarray:
        """年化波动率"""
        ...
    
    def downside_vol(self) -> np.ndarray:
        """下行波动率"""
        ...
    
    def downside_risk(self) -> np.ndarray:
        """用于计算sortino的下行标准差计算方式"""
        ...
    
    def max_drawback(self) -> np.ndarray:
        """最大回撤"""
        ...

    def sharpe(self) -> np.ndarray:
        """夏普比"""
        ...
    
    def calmar(self) -> np.ndarray:
        """卡玛比"""
        ...

    def sortino(self) -> np.ndarray:
        """索提诺值"""
        ...

    def var(self, days=None, alpha=0.05) -> np.ndarray:
        ...

    def cvar(self, days=None, alpha=0.05) -> np.ndarray:
        ...
    
    def perf(self) -> pd.DataFrame:
        """show all performance indicator"""
        ...
