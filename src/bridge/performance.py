from typing import List

import numpy as np
import pandas as pd


NO_RISK_RATIO = 0.015


class Performance:

    def __init__(self, data: pd.DataFrame):
        self.columns = data.columns
        self.data = data.values

    @staticmethod
    def from_navs(navs: List) -> "Performance":
        data = [{"date": x.date, "nav": x.nav} for x in navs]
        df = pd.DataFrame(data).set_index("date")
        return Performance(df)
    
    def pct_change(self) -> np.ndarray:
        return np.diff(self.data, axis=0) / self.data[:-1]

    def growth(self) -> np.ndarray:
        """累计收益"""
        s =  self.data[-1] / self.data[0] - 1
        s = np.round(s, 4)
        return s

    def cagr(self) -> np.ndarray:
        """复合增长率"""
        s = np.power(self.growth() + 1, 250 / len(self.data)) - 1
        s = np.round(s, 4)
        return s

    def vol(self) -> np.ndarray:
        s = self.pct_change().std(axis=0) * np.sqrt(250)
        s = np.round(s, 4)
        return s
    
    def downside_vol(self) -> np.ndarray:
        pct = self.pct_change()
        pct[pct < 0] = 0
        s = np.sqrt(np.power(pct, 2).sum(axis=0) / len(pct)) * np.sqrt(250)
        s = np.round(s, 4)
        return s
    
    def downside_risk(self) -> np.ndarray:
        """用于计算sortino的下行标准差计算方式"""
        pct = self.pct_change()
        pct = pct - NO_RISK_RATIO / 365
        pct[pct < 0] = 0
        s = np.sqrt((np.power(pct, 2).sum(axis=0) / len(pct)) * 250)
        return s
    
    def max_drawback(self) -> np.ndarray:
        s = (self.data / np.maximum.accumulate(self.data, axis=0) - 1).min(axis=0)
        s = np.round(s, 4)
        return s

    def sharpe(self) -> np.ndarray:
        """夏普比"""
        annual_return = self.cagr()
        volatility = self.vol()
        sharpe = (annual_return - NO_RISK_RATIO) / volatility
        sharpe = np.round(sharpe, 2)
        return sharpe
    
    def calmar(self) -> np.ndarray:
        """卡玛比"""
        annualized_return = self.cagr()
        drawback = self.max_drawback()
        calmar = -annualized_return / drawback
        calmar = np.round(calmar, 2)
        return calmar

    def sortino(self) -> np.ndarray:
        """索提诺值"""
        annual_return = self.cagr()
        dr = self.downside_risk()
        sortino = (annual_return - NO_RISK_RATIO) / dr
        sortino = np.round(sortino, 2)
        return sortino

    def var(self) -> np.ndarray:
        pct = self.pct_change()
        s = np.apply_along_axis(var, 0, pct)
        s = np.round(s, 4)
        return s

    def cvar(self) -> np.ndarray:
        pct = self.pct_change()
        s = np.apply_along_axis(cvar, 0, pct)
        s = np.round(s, 4)
        return s
    
    def perf(self) -> pd.DataFrame:
        methods = (
            "growth", "cagr", "vol", "downside_vol", "max_drawback", "sharpe", "calmar", "sortino", "var", "cvar"
        )
        data = []
        for method in methods:
            d = getattr(self, method)()
            data.append(d)
        
        data = np.array(data)
        df = pd.DataFrame(data, index=methods, columns=self.columns).T
        return df


def var(hist_change: np.ndarray, days=None, alpha=0.05) -> float:
    """
    计算var
    """
    if days:
        hist_change = hist_change[-days:]
    p = np.percentile(hist_change, alpha * 100, method="midpoint")
    return p


def cvar(hist_change: np.ndarray, days=None, alpha=0.05) -> float:
    """计算CVAR"""
    if days:
        hist_change = hist_change[-days:]
    hist_change = sorted(hist_change)
    p = np.percentile(hist_change, alpha * 100, method="midpoint")
    data = [x for x in hist_change if x < p]
    return sum(data) / len(data)
