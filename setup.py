import glob
from setuptools import setup
from Cython.Build import cythonize


core = cythonize(["src/bridge/*.py", "src/bridge/core/*.pyx"], language_level=3)


setup(
    name="bridge",
    ext_modules=core,
)
